class Walker
{
   float xPosition;
   float yPosition;
   
   Walker()
   {
      xPosition = 0;
      yPosition = 0;
   }
   
   Walker(float x, float y)
   {
    xPosition = x;
    yPosition = y;
   }
   
   void render()
   {     
     circle(xPosition, yPosition, 15);
     fill(random(255), random(255), random(255), random(255));
   }
   
   void walk()
   {
     int direction = floor(random(8));
     
     if(direction == 0)
     {
      yPosition += 5; 
     }
     else if(direction == 1)
     {
      yPosition -= 5; 
     }
     else if(direction == 2)
     {
      xPosition += 5;
     }
     else if(direction == 3)
     {
      xPosition -= 5; 
     }
     else if(direction == 4)
     {
      yPosition += 5;
      xPosition -= 5;
     }
     else if(direction == 5)
     {
      yPosition += 5;
      xPosition += 5;
     }
     else if(direction == 6)
     {
      yPosition -= 5;
      xPosition -= 5;
     }
     else if(direction == 7)
     {
      yPosition -= 5;
      xPosition += 5;
     }
     
   }
}
