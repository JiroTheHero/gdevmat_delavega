void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
    background(255);
}

Walker walker = new Walker();

void draw() //get called every frame
{
  //background(255);
  
  walker.render();
  walker.walk();
}
