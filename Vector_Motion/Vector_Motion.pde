Mover mover;

void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
    
    background(0);
    
    mover = new Mover();
    mover.position.x = Window.left;
    mover.acceleration = new PVector(0.25, 0);
}

void draw()
{
  background(0);
  
  mover.render();
  
  if(mover.position.x > Window.right)
  {
    mover.position.x = Window.left;
  }
}
