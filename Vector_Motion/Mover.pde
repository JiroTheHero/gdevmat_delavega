public class Mover
{
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  
  public float scale = 30;
  
  public void render()
  {
    update(); 
    circle(position.x, position.y, scale);
  }
  
  private void update()
  {
    if (this.position.x < 0)
    {
     this.velocity.add(this.acceleration);
    }
    else if (this.position.x > 0)
    {
      this.velocity.sub(this.acceleration);
    }
    
    this.position.add(this.velocity);
  }
}
