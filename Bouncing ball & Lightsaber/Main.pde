void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
    
    background(0);
}

Ball ball = new Ball();
Walker ball2 = new Walker();

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  
  return new Vector2(x ,y);
}

  float x_t = 15;
  float y_t = 20;
  float r_t = 50;
  float g_t = 30;
  float b_t = 20;

void draw()
{
  background(0);
  //ball.render();
  //ball2.render();
  //Vector2 mouse = mousePos();
  //Vector2 mouse2 = mousePos();
  
  fill(0);
  strokeWeight(12);
  circle(0, 0, 55);
  
  Vector2 position = new Vector2();
  
  float xn = noise(x_t);
  position.x = map(xn, 0, 1, Window.left, Window.right);
    
  float yn = noise(y_t);
  position.y = map(yn, 0, 1, Window.bottom, Window.top);
  
  position.normalize();
  position.mul(350);
  
  float rn = noise(r_t);
  float r = map(rn, 0, 1, 0, 255);
    
  float gn = noise(g_t);
  float g = map(gn, 0, 1, 0, 255);
    
  float bn = noise(b_t);
  float b = map(bn, 0, 1, 0, 255);
    
  strokeWeight(10);
  stroke(r, g, b);
  line(-position.x, -position.y, position.x, position.y);
  //filter (BLUR, 5); *the program slows down too much when I use filter but it works*
  
  strokeWeight(3);
  stroke(240);
  line(-position.x , -position.y, position.x, position.y);
  //filter(BLUR,2); *the program slows down too much when I use filter but it works*
  
  position.normalize();
  position.mul(40);
  strokeWeight(15);
  stroke(50);
  line(-position.x, -position.y, position.x, position.y);
  
  x_t += 0.01;
  y_t += 0.01;
  r_t += 0.01;
  g_t += 0.01;
  b_t += 0.01;
  println(position.mag());
}
