class Walker
{
   Vector2 position;
   
   Walker()
   {
    position = new Vector2();
   }
   
   Walker(float x, float y)
   {
    position = new Vector2(x, y);
   }
   
   Walker(Vector2 position)
   {
    this.position = position; 
   }
   
   void render()
   {     
     circle(position.x, position.y, 55);
     //fill(random(255), random(255), random(255), random(255));
     randomWalk();
   }
   
   public void randomWalk()
   {
     float decision = random(0, 4);
     
     if(decision == 0)
     {
       position.x++;
     }
     else if(decision == 1)
     {
       position.x--;
     }
     else if(decision == 2)
     {
       position.y++;
     }
     else if(decision == 3)
     {
       position.y--;
     }
   }
  
}
