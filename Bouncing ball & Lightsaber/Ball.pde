class Ball
{
  Vector2 position;
  Vector2 velocity;
  Ball()
  {
    position = new Vector2();
    velocity = new Vector2(10, 8);
  }
  
  float r_t = 10;
  float g_t = 30;
  float b_t = 50;
  
  void render()
  {
    float rn = noise(r_t);
    float r = map(rn, 0, 1, 0, 255);
    
    float gn = noise(g_t);
    float g = map(gn, 0, 1, 0, 255);
    
    float bn = noise(b_t);
    float b = map(bn, 0, 1, 0, 255);
    
    fill(r, g, b, 255);
    circle(position.x, position.y, 50);
    
    r_t += 0.01;
    g_t += 0.01;
    b_t += 0.01;
    move();
  }
  
  void move()
  {
    position.add(velocity);
    if (position.x > Window.right || position.x < Window.left)
    {
      velocity.x *= -1;
    }
    
    if (position.y > Window.top || position.y < Window.bottom)
    {
      velocity.y *= -1;
    }
  }
}
