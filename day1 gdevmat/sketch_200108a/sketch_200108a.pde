void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
}

void draw() //get called every frame
{
  //circle(0, 0, 50);
  background(255);
  drawCartesianPlane();
  //drawLinearFunction();
  //drawQuadraticFunction();
  //drawCircle();
  drawSinWave();
}

void drawCartesianPlane()
{
  line(300, 0, -300, 0);
  line(0, 300, 0, -300);

  for (int i = -300; i <= 300; i +=10)
  {
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  }
}

void drawLinearFunction()
{
  /*
    f(x) = x + 2
   Let x be 4, then y = 6 (4, 6)
   Let x be -5, then y = -3 (-5, -3)
   */

  for (int x = -200; x <= 200; x++)
  {
    circle(x, x + 2, 1);
  }
}

void drawQuadraticFunction()
{
  /* 
   f(x) = 10x^2 + 2x - 5
   Let x be 2, then y = 3
   Let x be -2, then y = 3
   LEt x be -1, then y = -6
   */

  for (float x = -300; x <= 300; x += 0.1)
  {
    circle(10 * x, (x * x) + (2 * x) - 5, 1);
  }
}

float radius = 50;

void drawCircle()
{
  for (int x = 0; x < 360; x++)
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 1);
  }
  //radius++;
}

float t = 0.0;
float dt = 8;
float amplitude = 50.0;
float frequency = 0.05;

void drawSinWave()
{
  for (int x = -300; x <= 300; x++)
  {
    circle(x, amplitude*sin(frequency * (t + x)), 2);
  }
  t += dt;
  
  if(keyPressed)
  {
    if(key == 'w')
    {
      amplitude+=5;
    }
    if (key == 's')
    {
      amplitude-=5;
    }
    if (key == 'a')
    {
      frequency -= 0.01;
    }
    if (key == 'd')
    {
      frequency += 0.01;
    }
  }
}
