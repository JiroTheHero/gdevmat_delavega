void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
    background(0);
}
//Matter blackHole = new Matter(random(Window.left,Window.right), random(Window.bottom, Window.top), 50);

Matter blackHole;
Matter matter[] = new Matter[100];

float val;
float sd;
float meanX;
float meanY;

void renderMatter()
{
  blackHole = new Matter(random(Window.left,Window.right), random(Window.bottom, Window.top), 50);
  blackHole.render();
  
  for(int i = 0; i < 100; i++)
  {
    val = randomGaussian();
    sd = 60;
    meanX = random(Window.left, Window.right);
    meanY = random(Window.bottom, Window.top);
    float x = (sd * val) + meanX;
    float y = (sd * val) + meanY;
    matter[i] = new Matter(x, y, random(20, 40));
    float rn = noise(random(255));
    float gn = noise(random(255));
    float bn = noise(random(255));
    float an = noise(random(255));
    matter[i].r = map(rn, 0 , 1, 0, 255);
    matter[i].g = map(gn, 0 , 1, 0, 255);
    matter[i].b = map(bn, 0 , 1, 0, 255);
    matter[i].a = map(an, 0 , 1, 0, 255);
    matter[i].render();
  }
}

void draw()
{
  /*Matter blackHole = new Matter(random(Window.left,Window.right), random(Window.bottom, Window.top), 50);
  blackHole.render();*/
  
  if (frameCount % 100 == 0)
  {
    background(0);
    renderMatter();
  }
  
  for(int i = 0; i < 100; i++)
  {
    PVector correctDirection = PVector.sub(blackHole.position, matter[i].position);
    correctDirection.normalize().mult(25);
    matter[i].position.add(correctDirection);
  }
  
}
