public class Matter
{
  public PVector position;
  float size;
  public float r = 255, g = 255, b = 255, a = 255;
  
  Matter()
  {
    position = new PVector();
  }
  
  Matter(float x, float y)
  {
    position = new PVector(x, y); 
  }
  
  Matter(float x, float y, float size)
  {
    position = new PVector(x, y); 
    this.size = size;
  }
  
  Matter(PVector position)
  {
    this.position = position; 
  }
  
  void render()
  {
    circle(position.x, position.y, size);
    fill(r, g, b, a);
    noStroke();
  }
}
