class Splatter
{
  float xPosition;
  float yPosition;
  
  Splatter()
  {
   xPosition = 0;
   yPosition = 0;
  }
  
  void render()
  {
    xPosition = generateRandGaussian(width);
    yPosition = random(height);
    float size = generateRandGaussian(1);
    
    noStroke();
    fill(random(256), random(256), random(256), random(256));
    circle(xPosition, yPosition, size);
  }
  
  float generateRandGaussian(float limit)
  {
    float value = randomGaussian();
    
    float sd = 60;
    float mean = limit/2;
    float x = (value * sd) + mean;
    
    return x;
  }
}
