void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  /*camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);*/
    background(255);
    frameRate(15);
}

Splatter paintSplat = new Splatter();

void draw() //get called every frame
{
 paintSplat.render();
 
 if (frameCount % 100 == 0)
 {
   background(255);
 }
}
