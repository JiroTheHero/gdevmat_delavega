void setup() //gets called when the program runs
{
  size(1080, 720, P3D);
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 
    0, 0, 0, 
    0, -1, 0);
    
    background(0);
}

Walker walker = new Walker();

//float dt = 0;// delta time

void draw()
{
  /*float n = noise(dt);
  float x = map(n, 0, 1, 0, Window.top);
  
  rect(Window.left + (dt * 100), Window.bottom, 1, x);
  dt += 0.01;*/
  
  walker.render();
  
  
}
