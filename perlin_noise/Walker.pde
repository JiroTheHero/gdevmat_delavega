class Walker
{
  float xPos;
  float yPos;
  ///float size;
  
  Walker()
  {
   xPos = 0;
   yPos = 0;
  }
  
  float x_t = 5;
  float y_t = 10;
  float r_t = 10;
  float g_t = 30;
  float b_t = 50;
  float s_t = 30;
  void render()
  {
    float xn = noise(x_t);
    float x = map(xn, 0, 1, Window.left, Window.right);
    
    float yn = noise(y_t);
    float y = map(yn, 0, 1, Window.bottom, Window.top);
    
    float rn = noise(r_t);
    float r = map(rn, 0, 1, 0, 255);
    
    float gn = noise(g_t);
    float g = map(gn, 0, 1, 0, 255);
    
    float bn = noise(b_t);
    float b = map(bn, 0, 1, 0, 255);
    
    float sn = noise(s_t);
    float s = map(sn, 0, 1, 20, 100);
    
    //noStroke();
    fill(r, g, b, 255);
    circle(x, y, s);
    
    x_t += 0.01;
    y_t += 0.01;
    r_t += 0.01;
    g_t += 0.01;
    b_t += 0.01;
    s_t += 0.01;
  }
}
